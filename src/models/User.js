// External Dependancies
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    token: {
      type: String,
    },
    username: {
      type: String,
    },
    password: String,
    first_name: String,
    last_name: String,
    display_name: String,
    age: Number,
    phone: String,
    role: String,
  },
  { versionKey: false }
);

module.exports = mongoose.model("user", userSchema, "Users");
