// External Dependancies
const mongoose = require("mongoose");

const apptSchema = new mongoose.Schema(
  {
    slot: {
      date: String,
      time: String,
      location: String,
      area: String,
      center: String
    },
    owner: Object,
    vehicle: Object,
    createdAt: Date,
    updatedAt: Date,
    appointmentDateTime: String
  },
  { versionKey: false }
);

module.exports = mongoose.model("Appointment", apptSchema, "Appointment");
