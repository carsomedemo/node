const Appointment = require("../models/Appointment");

const DbModel = {
  Appointment: Appointment,
};

function errorHandler(mongoError) {
  let computedErrors = [];

  if (!is_empty(mongoError["reason"])) {
    // Type Error : Detected 1
    const { kind, value, path, reason } = mongoError;
    computedErrors.push(
      `Field: ${path}, expected *${kind}*; receive '${value}'`
    );
  }

  if (!is_empty(mongoError["errors"])) {
    // Validations
    for (var field in mongoError["errors"]) {
      const { message, kind, path, value } = mongoError["errors"][field];
      computedErrors.push(
        `${message}
        (Field: ${path}, expected *${kind}*; receive '${value})'`
      );
    }
  }
  computedErrors = computedErrors.join(", ");
  return computedErrors;
}

const mod_db = {
  dbGetAll: async function (table, params) {
    return params
      ? await DbModel[table].find(params).sort({ createdAt: "desc" })
      : await DbModel[table].find({}).sort({ createdAt: "desc" });
  },
  dbGet: async function (table, key) {
    return await DbModel[table].findById(key);
  },
  dbGetOne: async function (table, params) {
    return await DbModel[table].findOne(params);
  },
  dbCreate: async function (table, params) {
    let result = {};

    let err = await new DbModel[table](params)
      .validate()
      .catch((errors) => errors);

    if (!is_empty(err)) {
      result["error"] = errorHandler(err);
    } else {
      result["data"] = await new DbModel[table](params).save();
    }
    return result;
  },
  dbUpdate: async function (params) {
    let options = { useFindAndModify: false, new: true };
    const { table, query, queryReq } = params;

    let result = {};

    let err = await new DbModel[table](queryReq)
      .validate()
      .catch((errors) => errors);

    if (!is_empty(err)) {
      result["error"] = errorHandler(err);
      result["data"] = await dbGetOne(table, query);
    } else {
      result["data"] = await DbModel[table].findOneAndUpdate(
        query,
        queryReq,
        options
      );
    }
    return result;
  },
};

module.exports = mod_db;
