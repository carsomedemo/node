const methods = {
  paginateSet: function(page, limit) {
    const option = {
      page: page || 1,
      limit: limit || 4,
      sort: { _id: -1 }
    };

    return option;
  }
};

module.exports = methods;
