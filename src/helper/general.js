const moment = require("moment");

const general_helper = {
  is_empty: function(value) {
    if (
      typeof value === "undefined" ||
      value === undefined ||
      value === null ||
      value === ""
    ) {
      return true;
    } else {
      if (typeof value === "object") {
        if (Object.keys(value).length === 0 && value.constructor === Object) {
          return true;
        }
      }
    }

    return false;
  },
  /*
   * return @string
   * Sun, Mon, Tue..
   */
  get_weekday: function(date) {
    return moment(new Date(date)).format("dddd");
  },
  /*
   * return @boolean
   */
  is_within_3_weeks: function(date) {
    return (
      moment(new Date(date)) >= moment(new Date()) &&
      moment(new Date(date)) <= moment(new Date()).add(21, "days")
    );
  },
  /*
   * return @boolean
   */
  is_within_hour: function(newDateTime, oldDateTime) {
    var newDateTime = moment(new Date(newDateTime));
    var oldDateTime = moment(new Date(oldDateTime));
    return Math.abs(newDateTime.diff(oldDateTime, "minutes")) < 60;
  }
};
module.exports = general_helper;
