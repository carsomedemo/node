// Import methods
const paginate = require("./paginate");

const methods = {
  ...paginate
  // ...paginate,
  // ...paginate
};

module.exports = methods;
