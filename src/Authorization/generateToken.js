const express = require("express");
const router = express.Router();
var jwt = require("jsonwebtoken");
const conf = require("../config/config");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const dotenv = require("dotenv").config();

const verifyUser = async function(req, user) {
  const { username, password } = req.body;
  const check = await bcrypt.compare(password, user.password);

  var valid = user;
  valid = valid && username === user.username;
  valid = valid && check === true;

  return valid;
};

router.post(
  "/generateToken",
  [
    check("username").isLength({ min: 1 }),
    check("password").isLength({ min: 1 })
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      const { username } = req.body;
      const user = await dbGetOne("User", { username });

      if (user) var valid = await verifyUser(req, user);

      if (!valid) {
        res.json("Invalid username or password!");
        return;
      }

      user.password = "";
      user.token = "";
      const expiresIn = 0;
      const data = user;
      var token = jwt.sign({ data }, process.env.privateKey);

      const params = {
        table: "User",
        query: { username },
        queryReq: { token }
      };
      dbUpdate(params);

      res.json({
        access_token: token,
        expires_in: expiresIn,
        token_type: conf.JWT_TOKEN_TYPE
      });
    } catch (e) {
      console.log("e", e);
      res.status(401).json({
        ok: false,
        error: {
          reason: "Registration Failed",
          code: 401,
          others: e.errmsg
        }
      });
    }
  }
);

module.exports = router;
