var jwt = require("jsonwebtoken");
const conf = require("../config/config");
const dotenv = require("dotenv").config();

module.exports = (req, res, next) => {
  req.session = {};

  var authHeader = req.headers.authorization;
  if (!authHeader || authHeader.indexOf(conf.JWT_TOKEN_TYPE) !== 0) {
    res.status(401).json("Authorization Error!");
    next();
  } else {
    let typeLength = conf.JWT_TOKEN_TYPE.length + 1;
    token = authHeader.substring(typeLength);

    jwt.verify(token, process.env.privateKey, async function(err, decoded) {
      if (err) {
        res.status(401).json("Invalid Token");
        next();
      } else {
        if (decoded) {
          decoded.data.token = token;
          req.user = decoded.data;

          next();
        } else {
          return res.status(401).json({
            ok: false,
            error: {
              reason: "Invalid Token",
              code: 401
            }
          });
        }
      }
    });
  }
};
