// middleware for doing role-based permissions
const permit = function(...allowed) {
  const isAllowed = role => allowed.indexOf(role) > -1;

  // return a middleware
  return (request, response, next) => {
    const { user } = request;
    const { role } = user;
    var valid = user;
    valid = valid && isAllowed(role);
    if (!valid) {
      // user is forbidden
      response.status(403).json({ message: "Forbidden" });
    }

    // role is allowed
    // so continue on the next middleware
    next();
  };
};

module.exports = permit;
