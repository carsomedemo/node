var express = require("express");
var bodyParser = require("body-parser");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
global["permit"] = require("./config/permission");
global["globalize"] = require("./helper/globalize");

const dbHelper = require("./helper/db_utils");
globalize(dbHelper);

const genHelper = require("./helper/general");
globalize(genHelper);

const responseHelper = require("./helper/response");
globalize(responseHelper);

const bookingRoute = require("./routes/bookingRoute");

const { DB_SERVER, DB_NAME, DB_CONFIG } = process.env;
const server = DB_SERVER;
const database = DB_NAME;
const config = DB_CONFIG;
const uri = server + database + config;

var app = express();
const router = express.Router();

app.use(bodyParser.json());

router.get("/", async (req, res) => {
  console.log("get_weekday", get_weekday("2020-06-22"));
  console.log("within_3_weeks", is_within_3_weeks("2020-07-18"));
  console.log(
    "is_within_hour",
    is_within_hour("2020-07-18 10:30:00", "2020-07-18 11:31:00")
  );
  res.json("hello world");
});

app.use(router);

app.use("/v1", bookingRoute);

// Connect to DB
mongoose
  .connect(uri)
  .then(() => console.log("MongoDB connected…"))
  .catch(err => console.log("err", err));

// Run the server!
const start = async () => {
  try {
    app.listen(3000, function() {
      console.log("listening on port 3000!");
    });
  } catch (err) {
    console.log("error");
  }
};
start();
