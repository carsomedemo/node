// Import our Controllers
const express = require("express");
const router = express.Router();
const requireHeader = require("../Authorization/requireHeader");
const conf = require("../config/config");
const bcrypt = require("bcrypt");
const { check, validationResult } = require("express-validator");
const dotenv = require("dotenv").config();

const hashPassword = async function (plainTextPassword) {
  let salt = await bcrypt.genSalt(parseInt(process.env.saltRound));
  return await bcrypt.hash(plainTextPassword, salt);
};

router.get(
  "/user/:username",
  requireHeader,
  permit("admin"),
  async (req, res) => {
    try {
      const id = req.params.id;
      const { username } = req.params;
      const result = await dbGetOne("User", { username });

      res.json(result);
    } catch (e) {
      res.json("Unexpected Server Error!");
    }
  }
);

router.post(
  "/user/add",
  [
    check("username").isLength({ min: 1 }),
    check("password").isLength({ min: 1 }),
  ],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      req.body.password = await hashPassword(req.body.password);

      let usr = await dbCreate("User", req.body);

      res.status(200).send({ ok: true });
    } catch (e) {
      console.log("e", e);
      res.status(401).send({
        ok: false,
        error: {
          reason: "Registration Failed",
          code: 401,
          others: e.errmsg,
        },
      });
    }
  }
);

router.put("/user/edit", requireHeader, async (req, res, next) => {
  try {
    if (req.user.token) {
      let token = req.user.token;
      const params = {
        table: "User",
        query: { token },
        queryReq: req.body,
      };
      let usr = await dbUpdate(params);
      if (is_empty(usr.error)) {
        usr.password = "";
        res.json(usr);
      } else {
        res.json(usr.error);
      }
    } else {
      res.status(401).send({
        ok: false,
        error: {
          reason: "Invalid Token",
          code: 401,
        },
      });
    }
  } catch (e) {
    next(e);
  }
});
module.exports = router;
