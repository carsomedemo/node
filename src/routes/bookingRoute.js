// Import our Controllers
const express = require("express");
const router = express.Router();
const requireHeader = require("../Authorization/requireHeader");
const conf = require("../config/config");
const bcrypt = require("bcrypt");
const { check, validationResult } = require("express-validator");
const dotenv = require("dotenv").config();
const moment = require("moment");

const validation = async (slot, owner) => {
  let { max_appointments } = checkOperatingHrs(slot);

  await checkSlot(slot, max_appointments);
  await checkBookingInSameHour(slot, owner);
};

const checkOperatingHrs = (slot, rejection = true) => {
  let { date, time } = slot;
  let day_of_week = get_weekday(date);
  let within_3_weeks = is_within_3_weeks(date);

  let error_flg = false;
  let max_appointments = 0;
  let errorMsg = "";

  switch (day_of_week) {
    case "Sunday":
      error_flg = true;
      errorMsg = "Carsome will rest on Sunday";
      break;

    case "Saturday":
      max_appointments = 4;
      errorMsg = "Carsome will have 4 inspection slots every 30mins";
      break;

    default:
      max_appointments = 2;
      errorMsg =
        "Carsome will have 2 inspection slots every 30mins, from 9AM to 6PM on weekdays.";
  }

  if (!within_3_weeks) {
    error_flg = true;
    errorMsg = "Users can only book the inspection for the next 3 weeks.";
  }

  if (
    moment(new Date(`${date} ${time}`)).format("HH:mm:ss") <
      moment(new Date(`${date} 09:00:00`)).format("HH:mm:ss") ||
    moment(new Date(`${date} ${time}`)).format("HH:mm:ss") >
      moment(new Date(`${date} 18:00:00`)).format("HH:mm:ss")
  ) {
    error_flg = true;
    errorMsg += "(from 9AM to 6PM on Saturday).";
  }

  if (error_flg && rejection) throw { msg: errorMsg };

  return { max_appointments };
};

const checkSlot = async (slot, max_appointments) => {
  let { date, time } = slot;
  let day_of_week = get_weekday(date);
  let errorMsg = "";

  let appts = await dbGetAll("Appointment", {
    "slot.date": moment(new Date(`${date} ${time}`)).format("YYYY-MM-DD"),
    "slot.time": {
      $gte: moment(new Date(`${date} ${time}`))
        .subtract(1, "minutes")
        .format("HH:mm:ss"),
      $lt: moment(new Date(`${date} ${time}`))
        .add(1, "minutes")
        .format("HH:mm:ss")
    }
  });

  // Max Slot
  if (appts && appts.length >= max_appointments) {
    errorMsg = `Inspection Slots are Full On ${date} ${time} (${day_of_week})`;
  }

  if (errorMsg) throw { msg: errorMsg };
};

const checkBookingInSameHour = async (slot, owner) => {
  let { name, email, phone } = owner;
  let { date, time } = slot;
  let hour = moment(new Date(`${date} ${time}`)).format("HH");

  let appts = await dbGetAll("Appointment", {
    $and: [
      {
        $or: [
          { "owner.name": name },
          { "owner.email": email },
          { "owner.phone": phone }
        ],
        "slot.date": moment(new Date(`${date} ${time}`)).format("YYYY-MM-DD"),
        "slot.time": {
          $gte: moment(new Date(`${date} ${hour}:00:00`)).format("HH:mm:ss"),
          $lt: moment(new Date(`${date} ${hour}:59:59`)).format("HH:mm:ss")
        }
      }
    ]
  });

  if (appts && appts.length >= 1)
    throw { msg: "Users cannot book the inspection slot in the same hour." };
};

const checkSlotsTaken = async (params, start_date, end_date) => {
  let { location, area, center } = params;

  start_date
    ? (start_date = moment(new Date(start_date)).format("YYYY-MM-DD"))
    : (start_date = moment(
        new Date().toLocaleString("en-US", {
          timeZone: "Asia/Kuala_Lumpur"
        })
      ).format("YYYY-MM-DD"));

  end_date
    ? (end_date = moment(new Date(end_date)).format("YYYY-MM-DD"))
    : (end_date = moment(new Date(end_date))
        .add(1, "months")
        .format("YYYY-MM-DD"));

  let appts = await dbGetAll("Appointment", {
    "slot.location": location,
    "slot.area": area,
    "slot.center": center,
    "slot.date": {
      $gte: moment(new Date(start_date))
        .subtract(1, "days")
        .format("YYYY-MM-DD"),
      $lt: moment(new Date(end_date))
        .add(1, "days")
        .format("YYYY-MM-DD")
    }
  });
  return appts;
};

const checkAvailability = async (params, startDate, endDate) => {
  let slotsTaken = await checkSlotsTaken(params, startDate, endDate);

  let availabilities = [];
  if (slotsTaken.length > 0) {
    slotsTaken.map(slots_t => {
      const { slot } = slots_t;
      const { location, area, center, date, time } = slot;
      const { max_appointments } = checkOperatingHrs(slot, false);

      if (!availabilities[date]) availabilities[date] = {};
      if (!availabilities[date][time]) availabilities[date][time] = {};
      if (!availabilities[date][time][location])
        availabilities[date][time][location] = {};
      if (!availabilities[date][time][location][area])
        availabilities[date][time][location][area] = {};

      if (!availabilities[date][time][location][area][center]) {
        availabilities[date][time][location][area][center] = {
          taken: 1,
          max_appointments
        };
      } else {
        availabilities[date][time][location][area][center]["taken"] += 1;
      }
    });
  }
  return availabilities;
};

router.post("/appointment/add", async (req, res, next) => {
  try {
    let { slot, vehicle, owner } = req.body;

    await validation(slot, owner);
    slot.date = moment(new Date(slot.date)).format("YYYY-MM-DD");

    let appt = await dbCreate("Appointment", {
      slot,
      vehicle,
      owner,
      createdAt: new Date().toLocaleString("en-US", {
        timeZone: "Asia/Kuala_Lumpur"
      })
    });

    res.status(200).send({
      code: "0001",
      message: "Appointment submitted successfully.",
      data: {
        appointmentId: appt.data._id
      }
    });
  } catch (e) {
    res.status(401).send({
      ok: false,
      error: {
        code: 401,
        info: "Appointment Failed",
        reason: e.msg
      }
    });
  }
});

router.get("/appointment", async (req, res) => {
  try {
    const id = req.params.id;
    const { username } = req.params;
    const appts = await dbGetAll("Appointment");

    res.json(appts);
  } catch (e) {
    res.json("Unexpected Server Error!");
  }
});

router.get("/appointment/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const appts = await dbGet("Appointment", id);

    res.json(appts);
  } catch (e) {
    res.json("Unexpected Server Error!");
  }
});

router.put("/appointment/edit", async (req, res, next) => {
  try {
    if (req.body.id) {
      const params = {
        table: "Appointment",
        query: { _id: req.body.id },
        queryReq: req.body
      };
      let appt = await dbUpdate(params);
      if (is_empty(appt.error)) {
        res.json(appt);
      } else {
        res.json(appt.error);
      }
    } else {
      res.status(401).send({
        ok: false,
        error: {
          reason: "Invalid Update",
          code: 401
        }
      });
    }
  } catch (e) {
    next(e);
  }
});

router.post("/appointment/datetime", async (req, res, next) => {
  let { location, area, center, date } = req.body;

  let startDate = date
    ? moment(new Date(date))
    : moment(
        new Date().toLocaleString("en-US", {
          timeZone: "Asia/Kuala_Lumpur"
        })
      );
  let endDate = startDate.clone().add(1, "months");

  try {
    let availabilities = await checkAvailability(req.body, startDate, endDate);
    let datesBetween = [];
    let current_hour = moment(
      new Date().toLocaleString("en-US", {
        timeZone: "Asia/Kuala_Lumpur"
      })
    )
      .add(1, "hours")
      .format("HH");

    if (parseInt(current_hour) >= 18) current_hour = "18";
    if (parseInt(current_hour) < 9) current_hour = "09";

    for (
      var m = moment(startDate);
      m.diff(endDate, "days") <= 0;
      m.add(1, "days")
    ) {
      let availableDate = {};
      availableDate.date = m.format("YYYY-MM-DD");
      availableDate.timeslot = [];
      for (
        var t = moment(`${availableDate.date} ${current_hour}:00:00`);
        t.diff(`${availableDate.date} 18:00:00`, "minutes") <= 0;
        t.add(30, "minutes")
      ) {
        let remaining = checkOperatingHrs(
          {
            date: availableDate.date,
            time: t.format("HH:mm:ss")
          },
          false
        )["max_appointments"];

        // Check Fully book
        if (
          availabilities[availableDate.date] &&
          availabilities[availableDate.date][t.format("HH:mm:ss")] &&
          availabilities[availableDate.date][t.format("HH:mm:ss")][location][
            area
          ][center]
        ) {
          let bookedSlot =
            availabilities[availableDate.date][t.format("HH:mm:ss")][location][
              area
            ][center];
          if (bookedSlot["taken"] >= bookedSlot["max_appointments"]) {
            continue;
          } else {
            remaining = bookedSlot["max_appointments"] - bookedSlot["taken"];
          }
        }

        availableDate.timeslot.push({
          value: t.format("HH:mm:ss"),
          remaining: remaining
        });
      }
      datesBetween.push(availableDate);
    }

    res.status(200).send({
      code: "0001",
      message: "Availabilities.",
      datesBetween: datesBetween,
      startDate: startDate,
      endDate: endDate
    });
  } catch (e) {
    res.status(401).send({
      ok: false,
      error: {
        code: 401,
        info: "Availability Failed",
        reason: e.msg
      }
    });
  }
});

module.exports = router;
